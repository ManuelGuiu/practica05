package programa;

import java.util.Scanner;

import clases.Arma;
import clases.Pistola;
import clases.Rifle;

/**
 * The Class Programa.
 */
public class Programa {
	
	/** The input. */
	static Scanner input = new Scanner(System.in);

	
	/**
	 * Metodo main.
	 *
	 * @param args argumentos
	 */
	public static void main(String[] args) {

		Arma arma1 = new Arma();
		Arma ak12 = new Arma("cod1", 94.5, 15, 1678.99, 3.5, "7'62x53", 680);
		Rifle rifle1 = new Rifle();
		Rifle m82a1 = new Rifle("cod2", 121.9, 40, 12050, 14, "12'7x99", 2000, 0, "Rifle de francotirador", "A1");
		Pistola pistola1 = new Pistola();
		Pistola m9 = new Pistola("cod3", 21.7, 16, 1500, 1, "9x19", 50, "Semi", "Angular-Pulgar");

		boolean continuar = true;

		while (continuar) {
			System.out.println("1. Acceder a un arma.");
			System.out.println("2. Acceder a un fusil.");
			System.out.println("3. Acceder a una pistola.");
			System.out.println("4. Salir.");

			int numero = comprobarEntero();

			switch (numero) {
			case 1:
				System.out.println("1. Mostrar datos.");
				System.out.println("2. Realizar comprobaciones.");
				numero = comprobarEntero();
				switch (numero) {
				case 1:
					System.out.println("1. Arma vacia-generica.");
					System.out.println("2. Arma registrada.");
					numero = comprobarEntero();
					switch (numero) {
					case 1:
						System.out.println(arma1);
						break;
					case 2:
						System.out.println(ak12);
						break;
					default:
						System.out.println("Introduce una opcion correcta.");
					}
					break;
				case 2:
					System.out.println("1. Arma vacia-generica.");
					System.out.println("2. Arma registrada.");
					numero = comprobarEntero();
					switch (numero) {
					case 1:
						System.out.println("1. Comprobar legalidad.");
						System.out.println("2. Recalibrar.");
						numero = comprobarEntero();
						switch (numero) {
						case 1:
							if (arma1.legalidad(arma1.getCodigoSerie())) {
								System.out.println("Legal.");
							} else {
								System.out.println("Ilegal.");
							}
							break;
						case 2:
							arma1.recalibrado();
							System.out.println("Arma recalibrada:");
							System.out.println(arma1);
							break;
						default:
							System.out.println("Introduce una opcion correcta.");
						}
						break;
					case 2:
						System.out.println("1. Comprobar legalidad.");
						System.out.println("2. Recalibrar.");
						numero = comprobarEntero();
						switch (numero) {
						case 1:
							if (ak12.legalidad(ak12.getCodigoSerie())) {
								System.out.println("Legal.");
							} else {
								System.out.println("Ilegal.");
							}
							break;
						case 2:
							ak12.recalibrado();
							System.out.println("Arma recalibrada:");
							System.out.println(ak12);
							break;
						default:
							System.out.println("Introduce una opcion correcta.");
						}
						break;
					default:
						System.out.println("Introduce una opcion correcta.");
					}
					break;
				}
				break;
			case 2:
				System.out.println("1. Mostrar datos.");
				System.out.println("2. Realizar comprobaciones.");
				numero = comprobarEntero();
				switch (numero) {
				case 1:
					System.out.println("1. Rifle vacio-generico.");
					System.out.println("2. Rifle registrado.");
					numero = comprobarEntero();
					switch (numero) {
					case 1:
						System.out.println(rifle1);
						break;
					case 2:
						System.out.println(m82a1);
						break;
					default:
						System.out.println("Introduce una opcion correcta.");
					}
					break;
				case 2:
					System.out.println("1. Rifle vacio-generico.");
					System.out.println("2. Rifle registrado.");
					numero = comprobarEntero();
					switch (numero) {
					case 1:
						System.out.println("1. Comprobar legalidad.");
						System.out.println("2. Recalibrar.");
						System.out.println("3. Recolocar accesorios.");
						numero = comprobarEntero();
						switch (numero) {
						case 1:
							if (rifle1.legalidad(rifle1.getCodigoSerie())) {
								System.out.println("Legal.");
							} else {
								System.out.println("Ilegal.");
							}
							break;
						case 2:
							rifle1.recalibrado();
							System.out.println("Arma recalibrada:");
							System.out.println(rifle1);
							break;
						case 3:
							System.out.println("Nuevo numero de accesorios:");
							numero = comprobarEntero();
							rifle1.modificacionArma(numero);
							rifle1.pesoTotal();
							System.out.println("Actualizado");
							System.out.println(rifle1);
							break;
						default:
							System.out.println("Introduce una opcion correcta.");
						}
						break;
					case 2:
						System.out.println("1. Comprobar legalidad.");
						System.out.println("2. Recalibrar.");
						System.out.println("3. Recolocar accesorios.");
						numero = comprobarEntero();
						switch (numero) {
						case 1:
							if (m82a1.legalidad(m82a1.getCodigoSerie())) {
								System.out.println("Legal.");
							} else {
								System.out.println("Ilegal.");
							}
							break;
						case 2:
							m82a1.recalibrado();
							System.out.println("Arma recalibrada:");
							System.out.println(m82a1);
							break;
						case 3:
							System.out.println("Nuevo numero de accesorios:");
							numero = comprobarEntero();
							m82a1.modificacionArma(numero);
							m82a1.pesoTotal();
							System.out.println("Actualizado");
							System.out.println(m82a1);
							break;
						default:
							System.out.println("Introduce una opcion correcta.");
						}
						break;
					default:
						System.out.println("Introduce una opcion correcta.");
					}
					break;
				}
				break;
			case 3:
				System.out.println("1. Mostrar datos.");
				System.out.println("2. Realizar comprobaciones.");
				numero = comprobarEntero();
				switch (numero) {
				case 1:
					System.out.println("1. Pistola vacia-generica.");
					System.out.println("2. Pistola registrada.");
					numero = comprobarEntero();
					switch (numero) {
					case 1:
						System.out.println(pistola1);
						break;
					case 2:
						System.out.println(m9);
						break;
					default:
						System.out.println("Introduce una opcion correcta.");
					}
					break;
				case 2:
					System.out.println("1. Pistola vacia-generica.");
					System.out.println("2. Pistola registrada.");
					numero = comprobarEntero();
					switch (numero) {
					case 1:
						System.out.println("1. Comprobar legalidad.");
						System.out.println("2. Recalibrar.");
						System.out.println("3. Cambiar Ergonomia.");
						numero = comprobarEntero();
						switch (numero) {
						case 1:
							if (pistola1.legalidad(pistola1.getCodigoSerie())) {
								System.out.println("Legal.");
							} else {
								System.out.println("Ilegal.");
							}
							break;
						case 2:
							pistola1.recalibrado();
							System.out.println("Arma recalibrada:");
							System.out.println(pistola1);
							break;
						case 3:
							System.out.println("Introduce la nueva ergonomia.");
							String nuevaErgo = input.nextLine();
							pistola1.cambioErgonomia(nuevaErgo);
							break;
						default:
							System.out.println("Introduce una opcion correcta.");
						}
						break;
					case 2:
						System.out.println("1. Comprobar legalidad.");
						System.out.println("2. Recalibrar.");
						System.out.println("3. Cambiar ergonomia.");
						numero = comprobarEntero();
						switch (numero) {
						case 1:
							if (m9.legalidad(m9.getCodigoSerie())) {
								System.out.println("Legal.");
							} else {
								System.out.println("Ilegal.");
							}
							break;
						case 2:
							m9.recalibrado();
							System.out.println("Arma recalibrada:");
							System.out.println(m9);
							break;
						case 3:
							System.out.println("Introduce la nueva ergonomia.");
							String nuevaErgo = input.nextLine();
							m9.cambioErgonomia(nuevaErgo);
							break;
						default:
							System.out.println("Introduce una opcion correcta.");
						}
						break;
					default:
						System.out.println("Introduce una opcion correcta.");
					}
					break;
				}
				break;
			case 4:
				continuar = false;
			}
		}
	}

	/**
	 * Comprobar entero, metodo te pide un numero, comprueba que es entero y lo devuelve, si no, se repite.
	 *
	 * @return the int
	 */
	public static int comprobarEntero() {
		boolean continuar = true;
		int numero = 0;
		String entrada = "";
		while (continuar) {
			continuar = false;
			entrada = input.nextLine();
			for (int i = 0; i < entrada.length(); i++) {
				if (entrada.charAt(i) <= '0' || entrada.charAt(i) >= '9') {
					continuar = true;
					System.out.println("Introduce un valor valido.");
					break;
				}
			}
		}
		numero = Integer.parseInt(entrada);
		return numero;
	}

}
