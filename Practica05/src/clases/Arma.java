package clases;

import java.util.Scanner;

/**
 * Class Arma, padre.
 * @author PORTATIL
 */
public class Arma {
	
	protected String codigoSerie;
	protected double largo;
	protected double alto;
	protected double valor;
	protected double peso;
	protected String calibre;
	protected double alcance;
	protected Scanner input = new Scanner(System.in);

	/**
	 * Constructor vacio.
	 */
	public Arma() {
		this.codigoSerie = "-";
		this.largo = 0;
		this.alto = 0;
		this.valor = 0;
		this.peso = 0;
		this.calibre = "-";
		this.alcance = 0;
	}

	/**
	 * Constructor que recibe todos los atributos como parametros.
	 * @param codigo codigo
	 * @param largo largo
	 * @param alto alto
	 * @param valor valor
	 * @param peso peso
	 * @param calibre calibre
	 * @param alcance alcance
	 */
	public Arma(String codigo, double largo, double alto, double valor, double peso, String calibre, double alcance) {
		this.codigoSerie = codigo;
		this.largo = largo;
		this.alto = alto;
		this.valor = valor;
		this.peso = peso;
		this.calibre = calibre;
		this.alcance = alcance;
	}

	/**
	 * Metodo devuelve codigo de serie.
	 *
	 * @return codigo serie
	 */
	public String getCodigoSerie() {
		return codigoSerie;
	}

	
	/**
	 * Cambia codigo serie.
	 *
	 * @param codigoSerie the new codigo serie
	 */
	public void setCodigoSerie(String codigoSerie) {
		this.codigoSerie = codigoSerie;
	}

	/**
	 * Metodo devuelve largo.
	 *
	 * @return largo
	 */
	public double getLargo() {
		return largo;
	}

	
	/**
	 * Cambia largo.
	 *
	 * @param largo the new largo
	 */
	public void setLargo(double largo) {
		this.largo = largo;
	}

	
	/**
	 * Devuelve alto.
	 *
	 * @return the alto
	 */
	public double getAlto() {
		return alto;
	}

	/**
	 * Cambia alto.
	 *
	 * @param alto the new alto
	 */
	public void setAlto(double alto) {
		this.alto = alto;
	}

	
	/**
	 * Devuelve valor.
	 *
	 * @return the valor
	 */
	public double getValor() {
		return valor;
	}

	/**
	 * Cambia valor.
	 *
	 * @param valor the new valor
	 */
	public void setValor(double valor) {
		this.valor = valor;
	}

	/**
	 * Devuelve peso.
	 *
	 * @return peso
	 */
	public double getPeso() {
		return peso;
	}

	/**
	 * Cambia peso.
	 *
	 * @param peso the new peso
	 */
	public void setPeso(double peso) {
		this.peso = peso;
	}

	/**
	 * Devuelve calibre.
	 *
	 * @return calibre
	 */
	public String getCalibre() {
		return calibre;
	}

	/**
	 * Cambia calibre.
	 *
	 * @param calibre the new calibre
	 */
	public void setCalibre(String calibre) {
		this.calibre = calibre;
	}

	/**
	 * Devuelve alcance.
	 *
	 * @return alcance
	 */
	public double getAlcance() {
		return alcance;
	}

	/**
	 * Cambia alcance.
	 *
	 * @param alcance the new alcance
	 */
	public void setAlcance(double alcance) {
		this.alcance = alcance;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return codigoSerie + " " + valor + " " + peso + " " + alto + " " + largo + " " + peso + " " + calibre + " "
				+ alcance;
	}

	/**
	 * Legalidad.
	 *
	 * @param codigo the codigo
	 * @return true, si es legal
	 */
	public boolean legalidad(String codigo) {
		String[] ilegales = { "cod1", "cod2", "cod3" };
		for (int i = 0; i < ilegales.length; i++) {
			if (codigo.equals(ilegales[i])) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Recalibrado, menu cambia los datos de calibre por unos predefinidos.
	 */
	public void recalibrado() {

		boolean continuar = true;
		while (continuar) {
			System.out.println("Seleciona nueva municion:");
			System.out.println("1. 7'62x39");
			System.out.println("2. 5'56x45");
			System.out.println("3. 9x19");
			System.out.println("4. .45 acp");
			int menu = 0;
			String entrada = "";
			while (continuar) {
				continuar = false;
				entrada = input.nextLine();
				for (int i = 0; i < entrada.length(); i++) {
					if (entrada.charAt(i) <= '0' || entrada.charAt(i) >= '9') {
						continuar = true;
						System.out.println("Introduce una opcion valida.");
						break;
					}
				}
			}
			menu = Integer.parseInt(entrada);

			switch (menu) {
			case 1:
				this.calibre = "7'62x39";
				continuar = false;
				break;
			case 2:
				this.calibre = "5'56x45";
				continuar = false;
				break;
			case 3:
				this.calibre = "9x19";
				continuar = false;
				break;
			case 4:
				this.calibre = ".45 acp";
				continuar = false;
				break;
			default:
				System.out.println("introduce una opcion valida");
				continuar = true;
			}
		}

	}
}
