package clases;
/**
 * Class Pistola, hijo.
 */
public class Pistola extends Arma {
	
	private String metodoDisparo;
	private String tipoErgonomia;

	/**
	 * Constructor que recibe todos los atributos como parametros.
	 *
	 * @param codigoSerie the codigo serie
	 * @param largo the largo
	 * @param alto the alto
	 * @param valor the valor
	 * @param peso the peso
	 * @param calibre the calibre
	 * @param alcance the alcance
	 * @param metodoDisparo the metodo disparo
	 * @param tipoErgonomia the tipo ergonomia
	 */
	public Pistola(String codigoSerie, double largo, double alto, double valor, double peso, String calibre,
			double alcance, String metodoDisparo, String tipoErgonomia) {
		super(codigoSerie, largo, alto, valor, peso, calibre, alcance);
		this.metodoDisparo = metodoDisparo;
		this.tipoErgonomia = tipoErgonomia;
	}

	/**
	 * Constructor vacio.
	 */
	public Pistola() {
		super();
		this.metodoDisparo = "-";
		this.tipoErgonomia = "-";
	}

	/**
	 * Cambio ergonomia, cambia ergonomia.
	 *
	 * @param nuevoErgo the nuevo ergo
	 */
	public void cambioErgonomia(String nuevoErgo) {
		this.tipoErgonomia = nuevoErgo;
	}
	
	/**
	 * Recalibrado, menu cambia valor calibre por otros predefinidos.
	 */
	public void recalibrado() {

		boolean continuar = true;
		while (continuar) {
			System.out.println("Seleciona nueva municion:");
			System.out.println("1. .22 Corto");
			System.out.println("2. .32 ACP");
			System.out.println("3. 9x14 R Galand");
			System.out.println("4. .38 Colt ACP");
			int menu = 0;
			String entrada = "";
			while (continuar) {
				continuar = false;
				entrada = input.nextLine();
				for (int i = 0; i < entrada.length(); i++) {
					if (entrada.charAt(i) <= '0' || entrada.charAt(i) >= '9') {
						continuar = true;
						System.out.println("Introduce una opcion valida.");
						break;
					}
				}
			}
			menu = Integer.parseInt(entrada);

			switch (menu) {
			case 1:
				this.calibre = ".22 Corto";
				continuar = false;
				break;
			case 2:
				this.calibre = ".32 ACP";
				continuar = false;
				break;
			case 3:
				this.calibre = "9x14 R Galand";
				continuar = false;
				break;
			case 4:
				this.calibre = ".38 Colt ACP";
				continuar = false;
				break;
			default:
				System.out.println("introduce una opcion valida");
				continuar = true;
			}
		}

	}
	
	/**
	 * Devuelve metodo disparo.
	 *
	 * @return the metodo disparo
	 */
	public String getMetodoDisparo() {
		return metodoDisparo;
	}

	/**
	 * Modifica metodo disparo.
	 *
	 * @param metodoDisparo the new metodo disparo
	 */
	public void setMetodoDisparo(String metodoDisparo) {
		this.metodoDisparo = metodoDisparo;
	}

	/**
	 * Devuelve tipo ergonomia.
	 *
	 * @return the tipo ergonomia
	 */
	public String getTipoErgonomia() {
		return tipoErgonomia;
	}

	/**
	 * Modifica tipo ergonomia.
	 *
	 * @param tipoErgonomia the new tipo ergonomia
	 */
	public void setTipoErgonomia(String tipoErgonomia) {
		this.tipoErgonomia = tipoErgonomia;
	}

	@Override
	public String toString() {
		return codigoSerie + " " + largo + " " + alto + " " + valor + " " + peso + " " + calibre + " " + alcance + " "
				+ metodoDisparo + " " + tipoErgonomia;
	}
}
