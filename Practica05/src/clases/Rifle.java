package clases;

/**
 * Class Rifle, hijo.
 */
public class Rifle extends Arma {

	private int nAccesorios;
	private String tipo;
	private String modelo;

	/**
	 * Constructor vacio.
	 */
	public Rifle() {
		super();
		this.nAccesorios = 0;
		this.tipo = "-";
		this.modelo = "-";
	}

	/**
	 * Constructor que recibe todos los atributos como parametros.
	 * @param codigoSerie the codigo serie
	 * @param largo the largo
	 * @param alto the alto
	 * @param valor the valor
	 * @param peso the peso
	 * @param calibre the calibre
	 * @param alcance the alcance
	 * @param nAccesorios the n accesorios
	 * @param tipo the tipo
	 * @param modelo the modelo
	 */
	public Rifle(String codigoSerie, double largo, double alto, double valor, double peso, String calibre, double alcance,
			int nAccesorios, String tipo, String modelo) {
		super(codigoSerie, largo, alto, valor, peso, calibre, alcance);
		this.nAccesorios = nAccesorios;
		this.tipo = tipo;
		this.modelo = modelo;
		pesoTotal();
	}

	/**
	 * Peso total, calcula el peso en funcion de los accesorios.
	 */
	public void pesoTotal() {
		for (int i = 1; i <= nAccesorios; i++) {
			peso = peso + 0.2;
		}
	}

	/**
	 * Modificacion arma, altera el numero de accesorios.
	 * @param nAccesorios the n accesorios
	 */
	public void modificacionArma(int nAccesorios) {
		this.nAccesorios = nAccesorios;
		pesoTotal();
	}

	/**
	 * Devuelve n accesorios.
	 *
	 * @return the n accesorios
	 */
	public int getnAccesorios() {
		return nAccesorios;
	}

	/**
	 * Modifica n accesorios.
	 *
	 * @param nAccesorios the new n accesorios
	 */
	public void setnAccesorios(int nAccesorios) {
		this.nAccesorios = nAccesorios;
	}

	/**
	 * Devuelve tipo.
	 *
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Modifica tipo.
	 *
	 * @param tipo the new tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Devuelve modelo.
	 *
	 * @return the modelo
	 */
	public String getModelo() {
		return modelo;
	}

	/**
	 * Modifica modelo.
	 *
	 * @param modelo the new modelo
	 */
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	@Override
	public String toString() {
		return codigoSerie + " " + largo + " " + alto + " " + valor + " " + peso + " " + calibre + " " + alcance + " "
				+ nAccesorios + " " + tipo + " " + modelo;
	}
}
